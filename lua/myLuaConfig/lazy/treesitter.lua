return {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function()
        require("nvim-treesitter.configs").setup({
            ensure_installed = {
                "vimdoc", "javascript", "typescript", "lua", "bash", "java"
            },

            auto_install =  true,
            
            indent = {
                enable = true
            },
        })
    end
}
