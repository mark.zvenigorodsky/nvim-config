return {
    'neovim/nvim-lspconfig',
    config = function()
        local lspconfig = require('lspconfig')
        lspconfig.tsserver.setup({})
        lspconfig.jdtls.setup({})
        end
}
